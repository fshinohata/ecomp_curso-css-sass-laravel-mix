@extends('layout')

@php ($title = 'Animations')

@section('css')
    <link rel="stylesheet" href="{{ mix('/css/animations.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/css-shapes.css') }}">
@endsection

@section('content')
    <div class="animation-box">
        <h1 class="animation-box__title">Rotating Yin Yang</h1>
        <div id="yin-yang" class="animate-rotate"></div>
    </div>

    <div class="animation-box">
        <h1 class="animation-box__title">Fast Rotating and Alternating Yin Yang</h1>
        <div id="yin-yang" class="animate-rotate-fast-alternate"></div>
    </div>

    <div class="animation-box">
        <h1 class="animation-box__title">Translating Yin Yang</h1>
        <div id="yin-yang" class="center animate-translate"></div>
    </div>

    <div class="animation-box">
        <h1 class="animation-box__title">Flipping Yin Yang</h1>
        <div id="yin-yang" class="animate-flip"></div>
    </div>

    <div class="animation-box">
        <h1 class="animation-box__title">Yin Yang That Zoom On Hover</h1>
        <div id="yin-yang" class="effect-zoom"></div>
    </div>
@endsection
