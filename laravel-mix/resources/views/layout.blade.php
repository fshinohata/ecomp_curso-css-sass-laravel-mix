<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curso de CSS/SASS</title>
    <link rel="icon" href="/favicon.ico">
    @yield('css')
</head>
<body>
    <nav class="navbar">
        <h1 class="navbar__title">{{ isset($title) ? $title : 'Curso de CSS/SASS' }}</h1>
    </nav>

    <main>
        @yield('content')
    </main>
</body>
</html>