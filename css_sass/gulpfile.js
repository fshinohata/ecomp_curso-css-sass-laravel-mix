var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var minifyJS = require('gulp-js-minify');
var concat = require('gulp-concat');

gulp.task('css', gulp.series(function(){
  gulp.watch('assets/sass/*.scss', function() {
    return gulp.src('assets/sass/*.scss')
      .pipe(sass())
      // .pipe(concat('style.css'))
      .pipe(minifyCSS())
      .pipe(gulp.dest('assets/dist/css'))
  });
}));

gulp.task('js', function(){
  gulp.watch('assets/js/*.js', function() {
	  return gulp.src('assets/js/*.js')
	  	.pipe(minifyJS())
	    .pipe(gulp.dest('assets/dist/js'))
  });
});

gulp.task('default', gulp.parallel([ 'css', 'js' ]));