var parallax = document.getElementsByClassName("parallax");
var docStyle = document.querySelector("style");

var i = 1;
for(var p of parallax)
{
	docStyle.innerHTML += `.image-${i} { background-image: url(${p.dataset.bgImage}); }\n`;
	p.classList.add(`image-${i}`);
	p.removeAttribute('data-bg-image');
	i++;
}